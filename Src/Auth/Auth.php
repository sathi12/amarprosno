<?php

namespace App\Auth;
use PDO;

class Auth
{
    private $email;
    private $password;

    public function setData($data = "")
    {
        $this->email = $data['email'];
        $this->password = $data['password'];
        return $this;
    }

    public function store()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=amarproshno_db', 'root', '');

            $stmt = $pdo->prepare('INSERT INTO users(id, email, password) VALUES(:id, :email, :password)');


            $stmt->execute(
                array(
                    ':id' => null,
                    ':email' => $this->email,
                    ':password' => $this->password
                )
            );
        }
        catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}