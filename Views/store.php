<?php

include_once ("../vendor/autoload.php");
use App\Utility\Utility;
use App\Auth\Auth;

$objAuth = new Auth();
$objAuth->setData($_POST)->store();